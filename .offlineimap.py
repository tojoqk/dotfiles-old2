from subprocess import check_output
import socket
import os

def get_pass(account):
    return check_output("pass email/" + account, shell=True).splitlines()[0]

def get_user(account):
    return check_output("pass email/" + account, shell=True).splitlines()[1].split()[1]

def get_imap(account):
    return check_output("pass email/" + account, shell=True).splitlines()[2].split()[1]

local = "localhost/" + socket.gethostname() + "/" + os.getlogin()
