;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules (gnu))
(use-service-modules desktop networking ssh mail xorg mcron)

(define masaya-hourly-job
  #~(job "10 * * * *"
         "/home/masaya/bin/hourly.sh"
         #:user "masaya"))

(operating-system
  (locale "en_US.utf8")
  (timezone "Asia/Tokyo")
  (keyboard-layout
    (keyboard-layout "us" "altgr-intl"))
  (host-name "nova")
  (users (cons* (user-account
                  (name "masaya")
                  (comment "Masaya Tojo")
                  (group "users")
                  (home-directory "/home/masaya")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  (packages
    (append
      (list (specification->package "nss-certs"))
      %base-packages))
  (services
    (append
      (list (service openssh-service-type
              (openssh-configuration
                (port-number 2496)
                (password-authentication? #f)
                (x11-forwarding? #t)))
            (service network-manager-service-type)
            (service wpa-supplicant-service-type)
            (dovecot-service #:config
                             (dovecot-configuration
                              (mail-location "maildir:%h/Maildir")))
            (service opensmtpd-service-type
                     (opensmtpd-configuration
                       (config-file (local-file "./smtpd.conf"))))
            (service mcron-service-type
                     (mcron-configuration
                      (jobs (list masaya-hourly-job)))))
      %base-services))
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (target "/dev/vda")
      (keyboard-layout keyboard-layout)))
  (swap-devices (list "/dev/vda2"))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device
               (uuid "44ece104-8c44-4409-92b3-e6f18a8d0221"
                     'ext4))
             (type "ext4"))
           %base-file-systems)))
