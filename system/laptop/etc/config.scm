;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules (gnu)
             (srfi srfi-1)
             (gnu packages ssh))
(use-service-modules desktop networking xorg docker mail audio)

(operating-system
  (locale "en_US.utf8")
  (timezone "Asia/Tokyo")
  (keyboard-layout
   (keyboard-layout "us"
                    #:options '("ctrl:nocaps")))
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (target "/dev/sda")
      (keyboard-layout keyboard-layout)))
  (swap-devices (list "/dev/sda2"))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device (file-system-label "my-root"))
             (type "ext4"))
           %base-file-systems))
  (host-name "laptop")
  (users (cons* (user-account
                  (name "masaya")
                  (comment "Masaya Tojo")
                  (group "users")
                  (home-directory "/home/masaya")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video" "docker")))
                %base-user-accounts))
  (groups (cons* (user-group
                   (name "docker")
                   (system? #t))
                 %base-groups))
  (packages
    (append
      (list (specification->package "docker-cli")
            (specification->package "docker-compose")
            (specification->package "containerd"))
      (map specification->package
           '("xfce4-notifyd"
             "xfce4-battery-plugin"
             "xfce4-systemload-plugin"
             "xfce4-whiskermenu-plugin"
             "xfce4-pulseaudio-plugin"
             "xfce4-mpc-plugin"
             "xfce4-mailwatch-plugin"
             "xfce4-places-plugin"
             "xfce4-timer-plugin"
             "xfce4-time-out-plugin"
             "xfce4-stopwatch-plugin"
             "xfce4-statusnotifier-plugin"
             "xfce4-netload-plugin"
             "xfce4-mount-plugin"
             "xfce4-fsguard-plugin"
             "xfce4-cpufreq-plugin"
             "xfce4-calculator-plugin"
             "xfce4-xkb-plugin"
             "xfce4-weather-plugin"
             "xfce4-wavelan-plugin"
             "xfce4-verve-plugin"
             "xfce4-smartbookmark-plugin"
             "xfce4-kbdleds-plugin"
             "xfce4-genmon-plugin"
             "xfce4-eyes-plugin"
             "xfce4-equake-plugin"
             "xfce4-embed-plugin"
             "xfce4-diskperf-plugin"
             "xfce4-datetime-plugin"
             "xfce4-cpugraph-plugin"
             "xfce4-clipman-plugin"
             "xfce4-screenshooter"
             "pavucontrol"
             ))
      (list (specification->package "nss-certs"))
      %base-packages))
  (services
    (append
      (list (service docker-service-type))
      (list (dovecot-service #:config
                             (dovecot-configuration
                              (mail-location "maildir:%h/MailDir"))))
      (list (set-xorg-configuration
             (xorg-configuration
              (keyboard-layout keyboard-layout)))
            (service gnome-desktop-service-type)
            (service xfce-desktop-service-type))
      %desktop-services)))
