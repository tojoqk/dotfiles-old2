#!/usr/bin/env -S guix environment --ad-hoc guile -- guile -e main -s
!#
(use-modules (srfi srfi-19)
             (ice-9 getopt-long))

(define srcdir (getenv "PWD"))
(define dstdir (getenv "HOME"))
(define backupdir (string-append srcdir
                                 "/backup/"
                                 (date->string (current-date)
                                               "~Y~m~d~H~M~S")))

(define (src-path name)
  (string-append srcdir "/" name))

(define (dst-path name)
  (string-append dstdir "/" name))

(define (backup-path name)
  (string-append backupdir "/" name))

(define (mkdir-p path)
  (let ((status-code (system* "mkdir" "-p" path)))
    (unless (zero? status-code)
      (error "mkdir-p return non zero status code" status-code))))

(define (backup-and-delete name)
  (format (current-error-port)
          "Since ~s exists, backed up to ~s~%"
          (dst-path name)
          (backup-path name))
  (mkdir-p (dirname (backup-path name)))
  (rename-file (dst-path name) (backup-path name)))

(define (symlink-exsits? path)
  (zero? (system* "test" "-L" path)))

(define (file-exists*? path)
  (or (file-exists? path)
      (symlink-exsits? path)))

(define (symlink-file name)
  (when (and (file-exists*? (dst-path name))
             (not (and (eq? 'symlink (stat:type (lstat (dst-path name))))
                       (string=? (src-path name)
                                 (readlink (dst-path name))))))
    (backup-and-delete name))
  (mkdir-p (dirname (dst-path name)))
  (unless (file-exists*? (dst-path name))
    (format #t "~s -> ~s~%" (src-path name) (dst-path name))
    (symlink (src-path name) (dst-path name))))

(define (symlink-files names)
  (for-each symlink-file names))

(define (pull)
  (let ((status-code (system* "guix" "pull")))
    (unless (zero? status-code)
      (exit status-code))))

(define (install-packages pkgs)
  (let ((status-code (apply system* "guix" "install" pkgs)))
    (unless (zero? status-code)
      (exit status-code))))

(define (fc-cache)
  (let ((status-code (system* "fc-cache" "-rv")))
    (unless (zero? status-code)
      (exit status-code))))

(define (main args)
  (let* ((option-spec '((skip-pull (value #f))
                        (skip-install (value #f))
                        (skip-fc-cache (value #f))))
         (options (getopt-long args option-spec))
         (skip-pull (option-ref options 'skip-pull #f))
         (skip-install (option-ref options 'skip-install #f))
         (skip-fc-cache (option-ref options 'skip-fc-cache #f)))
    (symlink-files (list ".emacs"
                         ".authinfo.gpg"
                         ".gnus.el"
                         ".gitconfig"
                         ".offlineimap.py"
                         ".offlineimaprc"
                         ".password-store"
                         ".bashrc"
                         ".bash_profile"
                         ".screenrc"
                         ".config/guix/channels.scm"
                         ".config/fontconfig/fonts.conf"
                         ".config/dunst/dunstrc"
                         ".stumpwmrc"))
    (unless skip-pull
      (pull))
    (unless skip-install
      (install-packages (list "guix"
                              "emacs"
                              "emacs-japanese-holidays"
                              "emacs-helm-projectile"
                              "emacs-prettier"
                              "emacs-helm-dash"
                              "emacs-paredit"
                              "emacs-restart-emacs"
                              "emacs-geiser"
                              "emacs-multi-term"
                              "emacs-org-journal"
                              "emacs-org-caldav"
                              "emacs-org-pomodoro"
                              "emacs-org-download"
                              "emacs-org-present"
                              "emacs-org-tree-slide"
                              "emacs-org-rich-yank"
                              "emacs-editorconfig"
                              "emacs-terraform-mode"
                              "emacs-undo-tree"
                              "emacs-use-package"
                              "emacs-exec-path-from-shell"
                              "emacs-ddskk"
                              "emacs-magit"
                              "emacs-w3m"
                              "emacs-helm-pass"
                              "emacs-pass"
                              "emacs-yaml-mode"
                              "emacs-guix"
                              "emacs-cyberpunk-theme"
                              "guile"
                              "nvi"
                              "git"
                              "screen"
                              "fontconfig"
                              "font-adobe-source-han-sans"
                              "font-adobe-source-sans-pro"
                              "font-adobe-source-serif-pro"
                              "font-hermit"
                              "gnupg"
                              "pinentry"
                              "wget"
                              "password-store"
                              "dunst"
                              "stumpwm")))
    (unless skip-fc-cache
      (fc-cache))))
