;; email and full-name
(setq user-mail-address "masaya@tojo.tokyo")
(setq user-full-name "Masaya Tojo")

;;; Backup files
(setq backup-directory-alist '(("." . "~/tmp/emacs/backups")))
(setq backup-by-copying t)

(use-package epa-file
  :config
  (epa-file-enable)
  (setq epa-file-select-keys nil))


(use-package japanese-holidays
  :config
  (setq calendar-holidays japanese-holidays)
  (setq mark-holidays-in-calendar t))

(use-package helm-config
  :init
  (use-package helm-config)
  :config
  (helm-mode 1)
  (global-set-key (kbd "M-x") 'helm-M-x)
  (global-set-key (kbd "C-x C-f") 'helm-find-files)
  (global-set-key (kbd "C-c p s f") 'helm-grep-do-git-grep)
  (helm-ff-cache-mode -1))

(use-package helm-projectile
  :init
  (setq projectile-keymap-prefix (kbd "C-c p"))
  :config
  (projectile-global-mode)
  (setq projectile-completion-system 'helm)
  (helm-projectile-on))

(use-package prettier-js
  :config
  (add-hook 'js2-mode-hook 'prettier-js-mode)
  (add-hook 'web-mode-hook 'prettier-js-mode)
  (add-hook 'yaml-mode-hook 'prettier-js-mode))

(use-package helm-dash
  :config
  (global-set-key (kbd "C-c d h") 'helm-dash)
  (global-set-key (kbd "C-c d a") 'helm-dash-activate-docset)
  (global-set-key (kbd "C-c d d") 'helm-dash-deactivate-docset)
  (global-set-key (kbd "C-c d i") 'helm-dash-install-docset)
  (global-set-key (kbd "C-c d u") 'helm-dash-install-user-docset))

(use-package paredit
  :config
  (autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
  (add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
  (add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
  (add-hook 'ielm-mode-hook             #'enable-paredit-mode)
  (add-hook 'lisp-mode-hook             #'enable-paredit-mode)
  (add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
  (add-hook 'scheme-mode-hook           #'enable-paredit-mode))

(use-package restart-emacs
  :config
  (global-set-key (kbd "C-c e r") 'restart-emacs))

(use-package geiser
  :config
  (add-to-list 'auto-mode-alist '("\\.sxml\\'" . scheme-mode))
  (setq geiser-implementations-alist
        '(((regexp "\\.scm$")
           guile)
          ((regexp "\\.sxml$")
           guile))))

(use-package multi-term
  :config
  (term-line-mode)
  (global-set-key (kbd "C-c t t")
                  (lambda ()
                    (interactive)
                    (if (get-buffer "*terminal<1>*")
                        (switch-to-buffer "*terminal<1>*")
                      (multi-term))))
  (global-set-key (kbd "C-c t c")
                  (lambda ()
                    (interactive)
                    (multi-term)))
  (global-set-key (kbd "C-c t n") 'multi-term-next)
  (global-set-key (kbd "C-c t p") 'multi-term-prev))

(use-package org
  :config
  (global-set-key (kbd "C-c l") 'org-store-link)
  (global-set-key (kbd "C-c a") 'org-agenda)
  (global-set-key (kbd "C-c c") 'org-capture)
  (global-set-key (kbd "C-c j") 'org-clock-goto)
  (setq org-crypt-disable-auto-save t)
  (setq org-agenda-include-diary t)
  "done"
  (setq org-todo-keywords
        '((sequence "TODO(t)" "WAIT(w)" "PROJECT(p)" "ALWAYS(a)" "|" "DONE(d)" "SOMEDAY(s)" "CANCELED(c)")))
  (defun org-journal-find-location ()
    (org-journal-new-entry t)
    (goto-char (point-min)))
  (let ((notes-file (concat org-directory "/notes.org.gpg"))
        (appintments-file (concat org-directory "/appointments.org.gpg")))
    (setq org-capture-templates
          `(("t" "Task" entry (file+headline "" "Tasks")
             "* TODO %?\n  %U\n")
            ("n" "Note" entry (file+headline "" "Tasks")
             "* TODO %?\n  %U\n  %a\n")
            ("s" "Schedule" entry (file+headline "" "Tasks")
             "* TODO %?\n   SCHEDULED: %^t\n  %U\n  %a\n")
            ("S" "Schedule" entry (file+headline "" "Tasks")
             "* TODO %?\n   SCHEDULED: %^T\n  %U\n  %a\n")
            ("D" "Deadline" entry (file+headline "" "Tasks")
             "* TODO %?\n   DEADLINE: %^T\n  %U\n  %a\n")
            ("a" "Appointment" entry (file ,appintments-file)
             "* %?\n  %^T\n")
            ("j" "Journal entry" entry (function org-journal-find-location)
             "* %(format-time-string org-journal-time-format)%?")))
    (setq org-default-notes-file notes-file)
    (setq org-agenda-files (list appintments-file
                                 notes-file)))
  (setq org-log-done t)
  (setq org-archive-location "%s_archive.gpg::")
  (advice-add 'org-archive-subtree :after #'org-save-all-org-buffers))

(use-package org-crypt
  :config
  (org-crypt-use-before-save-magic)
  (setq org-tags-exclude-from-inheritance '("crypt"))
  (setq org-crypt-key "masaya@tojo.tokyo"))

(use-package org-journal
  :config
  (customize-set-variable 'org-journal-dir
                          (concat org-directory "/journal/"))
  (customize-set-variable 'org-journal-date-format "%A, %d %B %Y")
  (setq org-journal-enable-agenda-integration t)
  (setq org-journal-enable-encryption t))

(use-package org-caldav
  :config
  (setq org-caldav-inbox (concat org-directory "/appointments.org.gpg"))
  (setq org-caldav-files (list org-caldav-inbox))
  (setq org-caldav-url "https://dav.mailbox.org/caldav")
  (setq org-caldav-calendar-id "Y2FsOi8vMC8zMQ")
  (setq org-icalendar-timezone "Asia/Tokyo")
  (setq org-icalendar-use-scheduled '(event-if-todo-not-done))
  (setq org-icalendar-use-deadline '(event-if-todo-not-done))
  (setq org-icalendar-include-sexps t)
  (global-set-key (kbd "C-c s") 'org-caldav-sync))

(use-package org-pomodoro)

(use-package org-download
  :config
  (add-hook 'dired-mode-hook 'org-download-enable)
  (setq org-download-screenshot-method "scrot -so /tmp/screenshot.png"))

(use-package org-present)

(use-package org-tree-slide
  :config
  (global-set-key (kbd "<f8>") 'org-tree-slide-mode)
  (global-set-key (kbd "S-<f8>") 'org-tree-slide-skip-done-toggle))

(use-package org-rich-yank
  :demand t
  :bind (:map org-mode-map
              ("C-M-y" . org-rich-yank)))

(use-package skk
  :config
  (setq default-input-method "japanese-skk"))

(use-package editorconfig)

(use-package terraform-mode
  :config
  (add-hook 'terraform-mode-hook 'terraform-format-on-save-mode))

(use-package undo-tree
  :config
  (global-undo-tree-mode t))

(use-package exec-path-from-shell
  :init
  (exec-path-from-shell-initialize)
  (exec-path-from-shell-copy-env "MASTODON_HOST")
  (exec-path-from-shell-copy-env "MASTODON_ACCESS_TOKEN"))

(load-file "~/src/guix/etc/copyright.el")
(setq copyright-names-regexp
      (format "%s <%s>" user-full-name user-mail-address))
(add-hook 'after-save-hook 'copyright-update)

(load-theme 'cyberpunk t)

(defun weblio (value)
  "Search world with weblio"
  (interactive "sweblio: ")
  (other-window 1)
  (w3m-goto-url (concat "https://ejje.weblio.jp/content/" value))
  (other-window -1))

(global-set-key (kbd "C-c w w") 'weblio)

(use-package w3m-search
  :config
  (add-to-list 'w3m-search-engine-alist '("duckduckgo" "http://duckduckgo.com/?q=%s"))
  (setq w3m-search-default-engine "duckduckgo"))

;;; Appearance
(setq inhibit-startup-screen t)
(display-battery-mode)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(fringe-mode 1)
(setq ring-bell-function 'ignore)

;;; Indent
(setq-default indent-tabs-mode nil)
(setq sh-basic-offset 2)

;;; visual-line-mode
(add-hook 'visual-line-mode-hook
          '(lambda()
             (setq word-wrap nil)))

(set-face-attribute 'default nil :family "Hermit" :height 140)
(set-fontset-font t 'japanese-jisx0208 "Source Han Sans")
(set-fontset-font nil 'symbol "Noto Color Emoji")

(setq-default geiser-guile-load-path '("~/src/guix"
                                       "~/src/toot"))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("4682e26d7147963b1fb9b38bc730eaf800bd745c596e8de652211e7f4ec2d643" default)))
 '(org-journal-date-format "%A, %d %B %Y")
 '(org-journal-dir "~/org/journal/")
 '(org-now-location (quote ("/home/masaya/org")))
 '(package-selected-packages (quote (helm-ag))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
