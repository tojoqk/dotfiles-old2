(setq gnus-select-method
      '(nnimap "mail.tojo.tokyo"
               (nnimap-stream network)
               (nnimap-authenticator login)
               (nnimap-stream ssl)))

(setq send-mail-function 'smtpmail-send-it
      message-send-mail-function 'smtpmail-send-it
      smtpmail-stream-type 'starttls
      smtpmail-smtp-server "mail.tojo.tokyo"
      smtpmail-smtp-service 587)

(add-to-list 'gnus-secondary-select-methods '(nnml ""))

(setq gnus-parameters
      (cons
       '(".*" (gcc-self . "Sent"))
       gnus-parameters))
