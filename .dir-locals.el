;; Per-directory local variables for GNU Emacs 23 and later.

((nil
  . ((fill-column . 78)
     (tab-width   .  8)
     (sentence-end-double-space . t)))
 (c-mode          . ((c-file-style . "gnu")))
 (scheme-mode
  .
  ((indent-tabs-mode . nil)
   (eval . (put 'operating-system 'scheme-indent-function 0))
   (eval . (put 'bootloader 'scheme-indent-function 0))
   (eval . (put 'file-systems 'scheme-indent-function 0))
   (eval . (put 'users 'scheme-indent-function 0))
   (eval . (put 'packages 'scheme-indent-function 0))
   (eval . (put 'services 'scheme-indent-function 0))
   (eval . (put 'keyboard-layout 'scheme-indent-function 0))

   ;; This notably allows '(' in Paredit to not insert a space when the
   ;; preceding symbol is one of these.
   (eval . (modify-syntax-entry ?~ "'"))
   (eval . (modify-syntax-entry ?$ "'"))
   (eval . (modify-syntax-entry ?+ "'"))))
 (emacs-lisp-mode . ((indent-tabs-mode . nil)))
 (texinfo-mode    . ((indent-tabs-mode . nil)
                     (fill-column . 72))))
